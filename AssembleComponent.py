# Assembly of a wall component of TheNewMakers https://www.thenewmakers.com/  
   
# Author: Jeroen de Bruijn  
# Email: J.deBruijn1991@gmail.com 

### INPUT

# PSEUDOCODE 1
# Variable for the robot name
RobotName = "Comau Smart5 NJ 110-3.0"

# Distance to move to on the selected frame Z axis when picking up an element
DistanceStartPos = 500
# Move distance over the selected frame Z axis before sliding an element in its final position
DistanceFinalPos = 450
# Move distance over the tool Z axis after sliding the element in its final position
DistanceTool = 50

# Variable for the frame in which the assembled component is located
FrameComp = "The component"

# Variable for the screwdriver tool
ScrewdriverGeom = "Screwdriver geometry"
ScrewdriveLoc = "Screwdriver location"



### FUNCTIONS

# PSEUDOCODE 2
# Function to reset the geometry
def ResetGeometry():
        # Show the screwdriver tool
        RDK.Item(ScrewdriverGeom).setVisible(True)
        # Define first part of the name of each element
        firstPart = "ELM."
        # Go trough each item
        for item in RDK.ItemList():
                if item.Valid():
                        name = item.Name()
                        # Check if the item is geometry
                        if item.Type() == 5:
                                # Check the name of the item to determine if it needs to be deleted
                                if name == tempGeom or name == "temp Screwdriver":
                                        # Delete the old stuff
                                        item.Delete()

                                # Check if first part of the item name matches with the beginning structure
                                if name[:len(firstPart)] == firstPart:
                                        # Show the geometry
                                        item.setVisible(True)

# Function to drill a screw
def screw(theItem):
        # Approach over the X axis of the screw tool
        approach = theItem.Pose()*transl(-DistanceTool,0,0)
        # Linear move to the approach position
        robot.MoveL(approach)
        # Move to the screw hole
        robot.MoveL(theItem)       
        # Linear move to the approach position
        robot.MoveL(approach)


### START OF SCRIPT

# PSEUDOCODE 3
# Import library to communicate with RoboDK
from robolink import *
# Import library for robotics toolbox
from robodk import *

# Name for temporary geometry
tempGeom = "temp OJB geometry"

# Any interaction with RoboDK must be done through RDK
RDK = Robolink()
# Call the function to reset the geometry
ResetGeometry()

# Select the robot
robot = RDK.Item(RobotName)
# Get its tools, make sure in RoboDK that the vacuum gripper is the first tool and the screwdrive second
robotGripper = robot.Childs()[0]
robotScrewdriver = robot.Childs()[1]
# Hide the screwdriver tool in case it might still be visible
robotScrewdriver.setVisible(False)
# Just to make sure, set the vacuum gripper as active tool
robot.setPoseTool(robotGripper)

# Set robot joints to home position otherwise the robot sometimes makes unexpected maneuvers between two points
# The home position can be set manually in RoboDK.
# For the Comau Smart5 NJ 110-3.0 this is recommended: robot.setJoints([0,0,-90,0,0,0])
robot.setJoints( robot.JointsHome() )
#robot.setJoints([0,0,-90,0,0,0])

# PSEUDOCODE 4
# Construct empty matrix (2d list) as long and wide as there are number of elements
# The matrix is to big, but will be made shorter later on
# If the matrix is to short the elements can not be placed on the right position in the matrix
items = [[None for x in range(len(RDK.ItemList()))] for y in range(len(RDK.ItemList()))]

# Define first part of the name of each element
firstPart = "ELM."
# Go trough each item
for item in RDK.ItemList():
        if item.Valid():
                name = item.Name()
                # Check if first part of the item name matches with the beginning structure
                if name[:len(firstPart)] == firstPart:
                        # Disregard the first part and split the string at a .
                        data = name[len(firstPart):].split(".")
                        # Add item to correct location in the matrix
                        items[ int(data[0]) ][ int(data[1]) ] = item

# Remove empty lists from matrix
items = list(filter(any, items))
# Remove empty items from lists in matrix
for i, row in enumerate(items):
        items[i] = [x for x in row if x is not None]

# PSEUDOCODE 5
# It is important to provide the reference frame and the tool frames when generating programs offline
robot.setPoseFrame(robot.PoseFrame())
robot.setPoseTool(robot.PoseTool())

# Retrieve the robot reference frame
robotReference = robot.Parent()

# Retrieve the assembled component reference frames
componentReference = RDK.Item(FrameComp)
# All  move actions should be made relative to assembled component reference
robot.setPoseFrame( componentReference )

# PSEUDOCODE 6
# Loop trough all list in the matrix
for aList in items:
        # Variable for the first item in the current list
        firstItem = aList[0]

        # PSEUDOCODE 6.1
        # Check if the first item in the list is geometry
        if firstItem.Type() == 5:
                # Determine last step number
                lastStep = len(aList[1:])-1
                # Loop trough each item, but skip first item because that is the geometry
                for i, item in enumerate(aList[1:]):
                        # Check if it is the first or last step
                        if i == 0 or i == lastStep:
                                if i == 0:
                                        # Copy item location
                                        approach = item.Pose()
                                        posItem = approach.Pos()
                                        # Set Z-value
                                        posItem[2] = DistanceStartPos
                                        # Update the approach location with the new Z-value so the approach will always have the same Z-value
                                        approach.setPos(posItem)
                                else:
                                        # Calculate approach position over the world Z axis
                                        approach = Offset(item.Pose(), 0, 0, DistanceFinalPos)
                                
                                # Joint move to the approach position
                                robot.MoveJ(approach)
                                # Linear move down to the item
                                robot.MoveL(item)
                        else:
                                # Joint move to the item
                                robot.MoveJ(item)
                        
                        # Check if it is the first point and the geometry need to be picked up
                        if i == 0:
                                # Copy the geometry
                                aList[0].Copy()
                                # Paste the geometry to the same parent and thus the same location
                                item_temp = RDK.Paste(aList[0].Parent())
                                # Rename the geometry
                                item_temp.setName(tempGeom)
                                # Connect the geometry with the robot tool without changing its absolute position
                                item_temp.setParentStatic(robotGripper)
                                
                                # Hide the geometry
                                aList[0].setVisible(False)
                                
                                # Linear move up to the approach position
                                robot.MoveL(approach)
                        elif i == lastStep:
                                # Connect the geometry with the finished component without changing its absolute position
                                item_temp.setParentStatic( componentReference )
                                
                                # Leave over the Z axis of the tool instead of the world Z axis
                                approachLeaveFirst = item.Pose()*transl(0,0,-DistanceTool)
                                # Linear move to the approach leave position
                                robot.MoveL(approachLeaveFirst)
                                # Calculate second approach position over the world Z axis
                                approachLeaveSecond = Offset(approachLeaveFirst, 0, 0, DistanceFinalPos)
                                # Linear move to the approach leave position
                                robot.MoveL(approachLeaveSecond)
        # PSEUDOCODE 6.2
        # Check if screws need to be added
        elif "SCREWS" in firstItem.Name():
                # Get location of the screwdriver tool
                locationScrew = RDK.Item(ScrewdriveLoc)
                # Copy location
                approach = locationScrew.Pose()
                posItem = approach.Pos()
                # Set Z-value
                posItem[2] = DistanceStartPos
                # Update the approach location with the new Z-value so the approach will always have the same Z-value
                approach.setPos(posItem)

                # Set the screwdriver as the active tool
                robot.setPoseTool(robotScrewdriver)
                # Joint move to the approach location
                robot.MoveJ(approach)
                # Linear move down to the location
                robot.MoveL(locationScrew)
                
                # Show screwdriver tool
                robotScrewdriver.setVisible(True)
                # Hide the screwdriver geometry
                RDK.Item(ScrewdriverGeom).setVisible(False)

                # Linear move up to the approach location
                robot.MoveJ(approach)

                # Loop trough all items in the list, except for the first item
                for item in aList[1:]:
                        # Check if screw is in the item name
                        if "SCREW" in item.Name():
                                # Call the screw function
                                screw(item)
                        else:
                                # Joint move to the item
                                robot.MoveJ(item)

                # Joint move to the approach location
                robot.MoveJ(approach)
                # Linear move down to the location where the screwdriver should be placed
                robot.MoveL(locationScrew)
                
                # Set the vacuum gripper as active tool
                robot.setPoseTool(robotGripper)
                 # Show the screwdriver geometry
                RDK.Item(ScrewdriverGeom).setVisible(True)
                # Hide screwdriver tool
                robotScrewdriver.setVisible(False)
                
                # Linear move up to the approach location
                robot.MoveL(approach)
        # PSEUDOCODE 6.3
        # Check if all geometry needs to be moved
        elif "ALL" in firstItem.Name():
                # Calculate approach location over the world Z axis
                approach = Offset(firstItem.Pose(), 0, 0, DistanceFinalPos)
                # Joint move to the approach location
                robot.MoveJ(approach)
                # Linear move down to the location
                robot.MoveL(firstItem)
                # Go trough each item in RoboDK
                for item in RDK.ItemList():
                        if item.Valid():
                                # Check if name matches the geometry
                                if item.Name() == tempGeom: 
                                        # Connect the item with the robot tool without changing its absolute position
                                        item.setParentStatic(robotGripper)

                # Linear move up to the approach location
                robot.MoveL(approach)

                # If the list contains 3 items or more, than it has points in between as well
                if len(aList)>=3:
                        # Loop trough all items in the list, except for the first and last
                        for item in aList[1:-1]:
                                # Joint move to the item
                                robot.MoveJ(item)

                # Get the last item
                lastItem = aList[-1]
                
                # Calculate approach location over the world Z axis
                approach = Offset(lastItem.Pose(), 0, 0, DistanceTool)
                # Joint move to the approach location
                robot.MoveJ(approach)
                # Linear move down to the location
                robot.MoveL(lastItem)

                # Go trough each item in RoboDK
                for item in RDK.ItemList():
                        if item.Valid():
                                # Check if name matches the geometry
                                if item.Name() == tempGeom: 
                                        # Connect the geometry with the finished component without changing its absolute position
                                        item.setParentStatic( componentReference )
                
                # Leave to approach location over the Z axis of the tool
                approach = lastItem.Pose()*transl(0,0,-2*DistanceTool)
                # Linear move away from the geometry over the Z axis of the tool
                robot.MoveL(approach)
		# PSEUDOCODE 6.4
        # If it is not any of the above, make a joint move to the item. It should be a single item.
        else:
                # Joint move to the first item and only item
                robot.MoveJ(firstItem)
